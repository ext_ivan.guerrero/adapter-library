import React from "react";
import logo from "./logo.svg";
import "./App.css";
import {
  DsButton,
  DsIcon,
  DsTag,
  DsTextarea,
} from "./ds-components";

function App() {
  return (
    <>
      <h1>Proyecto TS (DS local)</h1>
      <DsIcon icon-name="check" />
    </>
  );
}

export default App;
