import { auto } from "@atomico/react";

import "@dscla/ds-tokens/tokens";
import "@dscla/ds-tokens/grid";
import "@dscla/ds-tokens/utils";
import "@dscla/ds-tokens/fonts";

// @ts-ignore
import { Button } from "@dscla/ds-button";
// @ts-ignore
import { Icon } from "@dscla/ds-icon";
// @ts-ignore
import { Tag } from "@dscla/ds-tag";
// @ts-ignore
import { TextArea } from "@dscla/ds-textarea";

const DsButton = auto(Button);
const DsIcon = auto(Icon);
const DsTag = auto(Tag);
const DsTextarea = auto(TextArea);

export { DsButton, DsIcon, DsTag, DsTextarea };
